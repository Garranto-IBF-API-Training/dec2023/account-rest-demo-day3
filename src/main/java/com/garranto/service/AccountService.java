package com.garranto.service;

import com.garranto.exceptions.AccountAlreadyExistException;
import com.garranto.exceptions.AccountNotPresentException;
import com.garranto.model.Account;
import com.garranto.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService implements AccountServiceSpec{

    @Autowired
    private AccountRepository accountRepository;
    @Override
    public List<Account> getAllAccounts() {
        List<Account> accountList = accountRepository.findAll();

//        if(search.equals("*")){
//
//        accountList = --------;
//        }

        return accountList;

    }

    @Override
    public Account getAccountByAccountNumber(String accNumber) throws AccountNotPresentException {


        Optional<Account> accountOptional = accountRepository.findById(accNumber);

        if(accountOptional.isPresent()){

            Account account = accountOptional.get();
            return account;
        }

        throw new AccountNotPresentException();

    }


    @Override
    public Account createANewAccount(Account newAccount) throws AccountAlreadyExistException {

        Optional<Account> accountOptional = accountRepository.findById(newAccount.getAccNumber());

        if(accountOptional.isPresent()){

            throw new AccountAlreadyExistException();
        }

        accountRepository.save(newAccount);
        return newAccount;
    }

    @Override
    public Account updateAccount(Account account, String accNumber) throws AccountNotPresentException {
        Optional<Account> accountOptional = accountRepository.findById(accNumber);

        if(accountOptional.isPresent()){

            account.setAccNumber(accNumber);
            accountRepository.save(account);
            return account;
        }

        throw new AccountNotPresentException();
    }
}


//optional is a container for any object to handle null values
//either be empty or contains the object if present
//

//using filters in spring boot
//interceptors
//aop
