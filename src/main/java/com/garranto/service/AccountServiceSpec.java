package com.garranto.service;

import com.garranto.exceptions.AccountAlreadyExistException;
import com.garranto.exceptions.AccountNotPresentException;
import com.garranto.model.Account;

import java.util.List;

public interface AccountServiceSpec {

    public List<Account> getAllAccounts();

    public Account getAccountByAccountNumber(String accNumber) throws AccountNotPresentException;

    public Account createANewAccount(Account account) throws AccountAlreadyExistException;

    public Account updateAccount(Account account, String accNumber) throws AccountNotPresentException;
}
