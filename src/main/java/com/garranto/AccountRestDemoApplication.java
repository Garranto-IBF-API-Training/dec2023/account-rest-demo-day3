package com.garranto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountRestDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountRestDemoApplication.class, args);
	}

}
