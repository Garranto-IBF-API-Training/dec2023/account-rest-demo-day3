package com.garranto.controller;

import com.garranto.dto.AccountBalance;
import com.garranto.dto.ResponseMessage;
import com.garranto.exceptions.AccountAlreadyExistException;
import com.garranto.exceptions.AccountNotPresentException;
import com.garranto.model.Account;
import com.garranto.service.AccountService;
import com.garranto.service.AccountServiceSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class AccountController {


    @Autowired
    private AccountServiceSpec accountService;

    @GetMapping(value = "/test")
    public String testHandler(){
        return "Welcome to Garranto";
    }


    @GetMapping(value = "/accounts")
    public ResponseEntity<List<Account>> getAllAccountsHandler( @RequestParam(name = "search",required = false) String search, @HEad){

        ResponseEntity responseEntity;

        List<Account> accountList = accountService.getAllAccounts();

        responseEntity = new ResponseEntity<List<Account>>(accountList, HttpStatus.OK);

//        read all the accounts from the accounts table
//        construct response entity with accounts read from the database
//        return the response entity


        return responseEntity;
    }

    @PostMapping(value = "/accounts")
    public ResponseEntity<?> addNewAccountHandler(@RequestBody Account newAccount){

        ResponseEntity<?> responseEntity;


        try {
            Account account = accountService.createANewAccount(newAccount);
            ResponseMessage success = new ResponseMessage("Success","Account created SuccessFully");
            responseEntity = new ResponseEntity<ResponseMessage>(success,HttpStatus.CREATED);
        } catch (AccountAlreadyExistException e) {
            ResponseMessage failure = new ResponseMessage("Failed","Account created Failed due to conflict");
            responseEntity = new ResponseEntity<ResponseMessage>(failure,HttpStatus.BAD_REQUEST);
        }


        return responseEntity;

    }

    @GetMapping(value = "/accounts/{accountNumber}")
    public ResponseEntity<?> getAccountByNumberHandler(@PathVariable("accountNumber") String accNumber){

        ResponseEntity<?> responseEntity;

        try {
            Account account = accountService.getAccountByAccountNumber(accNumber);
            responseEntity = new ResponseEntity<Account>(account, HttpStatus.OK);

        } catch (AccountNotPresentException e) {
            responseEntity = new ResponseEntity<String>("Account Not Found",HttpStatus.NOT_FOUND);
        }


        return responseEntity;

    }


    @GetMapping(value = "/accounts/balanceEnquiry")
    public ResponseEntity<?> balanceEnquiryHandler(@RequestParam(name = "accountNumber", required = true) String accNumber){

        ResponseEntity<?> responseEntity;

        try {
            Account account = accountService.getAccountByAccountNumber(accNumber);
            AccountBalance accountBalance = new AccountBalance(account.getBalance(),account.getCurrency());
            responseEntity = new ResponseEntity<AccountBalance>(accountBalance, HttpStatus.OK);

        } catch (AccountNotPresentException e) {
            responseEntity = new ResponseEntity<String>("Balance Enquiry Failed. Account Not Found",HttpStatus.NOT_FOUND);
        }


        return responseEntity;

    }

    @PutMapping(value = "/accounts/{accountNumber}")
    public ResponseEntity<?> updateAccountByNumberHandler(@PathVariable("accountNumber") String accNumber, @RequestBody Account account){

        ResponseEntity<?> responseEntity;

        try {
            Account updatedAccount = accountService.updateAccount(account, accNumber);
            responseEntity = new ResponseEntity<Account>(account, HttpStatus.OK);

        } catch (AccountNotPresentException e) {
            responseEntity = new ResponseEntity<String>("Updation Failed. Account Not Found",HttpStatus.NOT_FOUND);
        }


        return responseEntity;

    }
}

//dynamic urls

//separation of concerns

//loosely coupled

//logic + user Interface


//accounts
//accounts/123 --  get

//balanceEnquiry   post

//List<Account>
//response code
//response headers

//REsposneEntity

//data transfer object